package com.mycompany.actividacotizacion;

import jodd.http.HttpException;
import org.junit.Test;
import static org.junit.Assert.*;

public class CryptoCotizacionReposioryTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        CryptoCotizacionReposiory instance = new CryptoCotizacionReposiory();
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_urlIncorrecta_httpException() {
        CryptoCotizacionReposiory instance = new CryptoCotizacionReposiory();
        instance.setUrl("https://min-api.cryptocompar.com/data/price?fsym=BTC&tsyms=USD");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

}
