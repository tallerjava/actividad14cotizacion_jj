package com.mycompany.actividacotizacion;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionServiceTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() {
        List<CotizacionRepository> repositorios = new ArrayList();
        repositorios.add(new CoinDeskCotizacionRepository());
        repositorios.add(new BinanceCotizacionRepository());
        repositorios.add(new SomosPNTCotizacionRepository());
        repositorios.add(new CryptoCotizacionReposiory());
        CotizacionService instance = new CotizacionService(repositorios);
        List<Cotizacion> resultadoObtenido = instance.obtenerCotizaciones();
        assertNotNull(resultadoObtenido);
    }

    @Test()
    public void obtenerCotizacion_servidorNoResponde_precioEnCero() {
        List<CotizacionRepository> repositorios = new ArrayList();
        CoinDeskCotizacionRepository coinDesk = new CoinDeskCotizacionRepository();
        coinDesk.setUrl("https://api.coindes.com/v1/bpi/currentprice.json");
        repositorios.add(coinDesk);
        repositorios.add(new BinanceCotizacionRepository());
        repositorios.add(new SomosPNTCotizacionRepository());
        repositorios.add(new CryptoCotizacionReposiory());
        CotizacionService instance = new CotizacionService(repositorios);
        List<Cotizacion> resultadoObtenido = instance.obtenerCotizaciones();
        assertEquals(resultadoObtenido.get(0).getPrecio(), 0.0f, 0);
    }

}
