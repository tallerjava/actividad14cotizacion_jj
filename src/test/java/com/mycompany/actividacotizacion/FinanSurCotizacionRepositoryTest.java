package com.mycompany.actividacotizacion;

import java.sql.SQLException;
import org.junit.Test;
import static org.junit.Assert.*;

public class FinanSurCotizacionRepositoryTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() throws ClassNotFoundException, SQLException {
        FinanSurCotizacionRepository instance = new FinanSurCotizacionRepository();
        Cotizacion resultadoObtenido = null;
        resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = SQLException.class)
    public void obtenerCotizacion_servicioNoResponde_SQLException() throws ClassNotFoundException, SQLException {
        FinanSurCotizacionRepository instance = new FinanSurCotizacionRepository();
        instance.setUsuario("Edeuterio");
        Cotizacion resultadoObtenido = null;
        resultadoObtenido = instance.obtenerCotizacion();
    }
}
