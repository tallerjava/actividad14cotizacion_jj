package com.mycompany.actividacotizacion;

import java.util.Date;

public class Cotizacion {

    private Date fecha;
    private double precio;
    private String moneda;
    private String proveedor;

    public Cotizacion(String proveedor, Date fecha, String moneda, double precio) {
        this.fecha = fecha;
        this.precio = precio;
        this.moneda = moneda;
        this.proveedor = proveedor;
    }

    public String getProveedor() {
        return proveedor;
    }

    public Date getFecha() {
        return fecha;
    }

    public double getPrecio() {
        return precio;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

}
