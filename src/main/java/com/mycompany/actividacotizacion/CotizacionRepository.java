package com.mycompany.actividacotizacion;

import java.sql.SQLException;

public abstract class CotizacionRepository {

    public abstract Cotizacion obtenerCotizacion() throws ClassNotFoundException, SQLException;

    public abstract String getNombre();

}
