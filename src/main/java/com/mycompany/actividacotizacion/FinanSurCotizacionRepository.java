package com.mycompany.actividacotizacion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FinanSurCotizacionRepository extends CotizacionRepository {

    private String nombre = "Finan Sur";
    private String usuario = "Taller";
    private String password = "1234";

    @Override
    public String getNombre() {
        return nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Override
    public Cotizacion obtenerCotizacion() throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.jdbc.Driver");
        Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/finansur", usuario, password);
        Statement statement = conexion.createStatement();
        ResultSet result = statement.executeQuery("select * from cotizacion where fecha_cotizacion = (select max(fecha_cotizacion) from cotizacion);");
        if (result.next()) {
            return new Cotizacion(nombre, result.getTimestamp(4), result.getString(2), result.getDouble(3));
        }
        return null;
    }
}
